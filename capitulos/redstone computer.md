[ ⬅ Voltar ](.../README.MD)
 # Redstone computer

<img src="https://i.imgur.com/8fEVl.jpg" width="700">

# Uma breve história do jogo

Minecraft é um jogo eletrônico sandbox de sobrevivência criado pelo desenvolvedor sueco Markus "Notch" Persson e posteriormente desenvolvido e publicado pela Mojang Studios, cuja propriedade intelectual foi obtida pela Microsoft em 2014. Lançado inicialmente em maio de 2009 como um projeto em desenvolvimento, seu lançamento completo ocorreu em novembro de 2011 para Microsoft Windows, macOS e Linux, sendo posteriormente relançado para uma ampla variedade de plataformas. É o jogo mais vendido de todos os tempos, vendendo mais de 200 milhões de cópias em todas as plataformas até maio de 2020, com mais de 126 milhões de jogadores ativos mensalmente.

# Sobre o projeto

O jogo conta com uma variedade quase infinita de criações, oque levou o engenheiro e dono do canal [Legomasta99](https://www.youtube.com/channel/UCm6CQ7_2jmt0q43aW5mDtVA) A tendar desenvolver um computador utilizando apenas os recursos do jogo. E como matéria prima ele utilizou um material chamado de "redstone", que é um recurso, que atua como um condutor de energia dentro do jogo, esse material também dá origem a outras ferramentas que atuam de forma binária (ligado ou desligado), e partindo desses recursos o engenheiro de computação começou a elaborar seu projeto.

Como já mencionado o computador foi construido inteiramente dentro do jogo, e com os recursos disponibilizados nele, o desenvolvimento durou aproximadamente 3 anos até ele estar completo.

Para a sua criação o computador o desenvolvedor utilizou uma arquitetura de 8bits.

<img src="https://www.electronicsforu.com/wp-contents/uploads/2017/12/1-19-696x751.jpg" width="400">

Sendo assim o computador possui algumas especificações:
- arquitetura e interface de 8 bits
- Modelo de dados baseado em registro
- Quatro núcleos (que consistem em uma ALU + unidade de controle + cache + algumas outras coisas) que são verdadeiramente independentes um do outro
- Cada núcleo pode ser executado em velocidades separadas (integração de velocidade de clock variável)
- 32 bytes (31 acessíveis) de ERAM de leitura dupla
- Cada núcleo tem 128 linhas de memória de programa (768 bytes por núcleo, 3,072 Kbytes de memória total de programa)
- Executa o conjunto de instruções personalizadas
- Pode escrever, compilar, carregar e executar programas neste computador que foram escritos com as revisões mais recentes da especificação de linguagem ARCISS com o Projeto de Compilador DRCHLL
- Conjunto aprimorado de IO para permitir melhor conectividade com outros periféricos.

# Funcionalidades
As funcionalidades do cumputador ainda sao bem restritas principalmente por conta da velocidade de clock do seu processador, porém atualmente o computador possui alguns programas demo como:

- 1.Demo Program #1: Fibonacci Program
- 2.Demo Program #2: Multiplication Program
- 3.Demo Program #4: "Random Drawing" Program

Que executam tarefas básicas como multiplicar, ou apresentar uma sequencia Fibonacci.

# Curiosidades

Para ter acesso a esse projeto você pode baixa-lo [clicando aqui](https://tinyurl.com/RC50v1-2main).
Ou entrar no [vídeo de apresentação](https://www.youtube.com/watch?v=SbO0tqH8f5I) e ver como tudo funciona.
O engenheiro teve também a ajuda de alguns colegas:
- 2. [Bennyscube](https://www.youtube.com/user/bennyscube)
- 3. [Properinglish19](https://www.youtube.com/user/Properin)
- 4. [Laurens Weyn](https://www.youtube.com/user/laurensweyn)​
- 5. [Newomaster](https://www.youtube.com/user/Newomaster​)
- 6. [Skupitup](https://www.youtube.com/user/skupitup​)
- 7. [n00b_asaurus](https://www.youtube.com/user/n00basau)

# Referencias:
- https://en.wikipedia.org/wiki/8-bit_computing
- https://pt.wikipedia.org/wiki/Minecraft
- https://www.electronicsforu.com/electronics-projects/simple-8-bit-computer-learning
- https://profjefer.wordpress.com/licenciatura-em-computacao-ufpr/disciplinas/fund-de-arquitetura-de-computadores/

[ ⬅ Voltar ](.../README.MD)




















