[ ⬅ Voltar ](.../README.MD)

# Evolução dos Computadores Pessoais e sua Interconexão

## Primeiro Computador Eletrônico:
Considerado o primeiro computador eletrônico, o ENIAC era um dispositivo pesava 30 toneladas e foi usado na Segunda Guerra para cálculos balísticos.

<img src="https://www.seesp.org.br/site/images/Eniac_mulheres_interna.jpg" width = 200>

## Década de 70
 o Kenbak-1 é um dos mais referenciados como primeiro computador. Bastante rústico, o sistema usava uma coleção de interruptores como mecanismo de entrada de dados e era mais análogo a um mainframe industrial do que a um desktop contemporâneo.
 Ao final da década o preço da modernidade era muito caro, fruto dessa fase inicial de computadores pessoais, apareceram o Commodore PET e o Apple II mais acessíveis ao público e também surgindo o primeiro processador da Intel, o Intel 8080


 <img src="http://s2.glbimg.com/SV_VwZwvHKIZSjLN5TDCE8exmc8=/695x0/s.glbimg.com/po/tt2/f/original/2016/04/01/apple-ii.jpg" width = 200>

 ## Década de 80
 O Apple II encorajou a então gigante IBM a investir na ideia de criar um novo formato de computador que, completo com todo o hardware necessário, pudesse ser vendido para pequenos negócios e também residências. Em um conjunto que incluía teclado, tela, sistema operacional que rodava em cassete e CPU Intel 8088, custando um total de R$ 17.750
 Outro destaque da década de 1980 foi o aparecimento dos primeiros laptops. Embora o IBM 5100 de 1976 seja o primeiro computador portátil, a semente do laptop moderno pode ser encontrada no Grid Compass de 1982, pesando total de 5 kilos.


<img src="https://2k8fuf10pw8w25f6ce428ify-wpengine.netdna-ssl.com/ModernComputer/Personal/images/Grid1101.jpg" width = 200>

## Década de 90
 O computador pessoal encontrou seu formato e experiências mais extravagantes de hardware. Além disso, o design ganhou uma nova cara: o PC passou a ser formado por gabinete, monitor, teclado e mouse.
 A partir desse período, as grandes evoluções apareceram na forma como o usuário interagia com o PC. O Windows 95 virou uma referência de sistema operacional ao promover a chegada de uma interface gráfica mais limpa e funcional. 
  A Internet também se tornou uma realidade e passou a promover acesso a conteúdo, informação e formas de comunicação que pareciam desafiar limites geográficos.


 <img src="https://www.oficinadanet.com.br/imagens/post/15961/750xNxtd_old-pc-windows-95.jpg.pagespeed.ic.885d080c4d.jpg" width = 200>

 ## Década de 2000
 Os computadores, ganharam muito com a chegada de revoluções em processadores, rompendo a casa dos GHz e ganhando vários núcleos. Além disso, telas de alta resolução em e notebooks e baterias que trouxeram ainda mais portabilidade levaram a modelos que puderam competir frente a frente com desktops.
 Esses avanços também levaram à criação dos smartphones, tablets, entre outros. A Apple se destacou nesse período.


<img src="https://oimparcial.com.br/app/uploads/2019/06/anos-90-pc_600x338.jpg" width = 200>

## Década de 2010
 Com inteligência artificial, IoT (Internet das coisas), produtos smart (TVs, relógios, tomadas, entre outros), a década de 10 expandiu a ideia de computador pessoal a um outro nível. 
 Hoje, o usuário acessa no PC conteúdos que podem ser vistos em smartphones, já que o acesso em um mesmo browser, por exemplo, fica associado à nuvem, e não mais ao dispositivo em si. Além da possiblidade de conectar a outros dispositivos por meio de Bluetooth, aplicativos que executam comando por voz.


 <img src="https://s2.glbimg.com/jG-RbXlRoVsDHrxKhem0vNSarI8=/1200x/smart/filters:cover():strip_icc()/i.s3.glbimg.com/v1/AUTH_08fbf48bc0524877943fe86e43087e7a/internal_photos/bs/2020/y/i/EZUeDlSxap1cOrwAf3oA/acer-nitro-5.jpg" width = 200>

 ## Referências:
 GARRETT, FELIPE; Dia da Informática: veja a evolução dos PCs ao longo das décadas ; https://www.techtudo.com.br/noticias/2019/08/dia-da-informatica-veja-a-evolucao-dos-pcs-ao-longo-das-decadas.ghtml

[ ⬅ Voltar ](.../README.MD)




  
