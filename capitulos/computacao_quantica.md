[ ⬅ Voltar ](.../README.MD)
# Computação quântica
## O que é?
A computação quântica é a ciência que estuda as aplicações das teorias e propriedades da mecânica quântica na Ciência da Computação. Dessa forma seu principal foco é o desenvolvimento do computador quântico.
<img src=" https://s4.static.brasilescola.uol.com.br/be/2020/03/computador-quantico-ibm.jpg" width="500">
###### IBM Q, o computador quântico de 20 qubits da IBM, apresentado em janeiro de 2019.

Na computação clássica o computador é baseado na arquitetura de Von Neumann que faz uma distinção clara entre elementos de processamento e armazenamento de dados, isto é, possui processador e memória destacados por um barramento de comunicação, sendo seu processamento sequencial.
Entretanto os computadores atuais possuem limitações, como por exemplo na área de Inteligência Artificial (IA), onde não existem computadores com potência ou velocidade de processamento suficiente para suportar uma IA avançada. Dessa forma surgiu a necessidade da criação de um computador alternativo dos usuais que resolvesse problemas de IA, ou outros como a fatoração em primos de números muito grandes, logaritmos discretos e simulação de problemas da Física Quântica.
A Lei de Moore afirma que a velocidade de um computador é dobrada a cada 12 meses. Assim sempre houve um crescimento constante na velocidade de processamento dos computadores. Entretanto essa evolução tem um certo limite, um ponto onde não será possível aumentar essa velocidade e então se fez necessária uma revolução significativa na computação para que este obstáculo fosse quebrado. E assim os estudos em Computação Quântica se tornaram muito importantes e a necessidade do desenvolvimento de uma máquina extremamente eficiente se torna maior a cada dia.
## História
A pesquisa para o desenvolvimento da computação quântica iniciou-se já na década de 50 quando pensavam em aplicar as leis da física e da mecânica quântica nos computadores. Em 1981 em uma conferência no MIT o físico Richard Feynman apresentou uma proposta para utilização de sistemas quânticos em computadores, que teriam então uma capacidade de processamento superior aos computadores comuns. Já em 1985, David Deutsch, da Universidade de Oxford, descreveu o primeiro computador quântico, uma Máquina de Turing Quântica, ele simularia outro computador quântico. Depois de Deutsch apenas em 1994 houve notícias da computação quântica, quando em Nova Jersey, no Bell Labs da AT&T, o professor de matemática aplicada Peter Shor desenvolveu o Algoritmo de Shor, capaz de fatorar grandes números numa velocidade muito superior à dos computadores convencionais. Em 1996, Lov Grover, também da Bell Labs, desenvolveu o Speedup, o primeiro algoritmo para pesquisa de base de dados quânticos. Nesse mesmo ano foi proposto um modelo para a correção do erro quântico. Em 1999 no MIT foram construídos os primeiros protótipos de computadores quânticos utilizando montagem térmica. No ano de 2007 surge o Orion, um processador quântico de 16 qubits que realiza tarefas praticas foi desenvolvido pela empresa canadense D-Wave. Em 2011 a D-Wave lançou o primeiro computador quântico para comercialização, o D-Wave One, que possui um processador de 128 qubits. Porém o D-Wave One ainda não é totalmente independente, precisa ser usado em conjunto com computadores convencionais. Em 2017, a D-Wave Systems lançou comercialmente o 2000Q, um computador quântico de 2000 qubits a módicos US$ 15 milhões.  O computador quântico anterior da companhia tinha 1.000 qubits.

<img src="https://i2.wp.com/engenheironaweb.com/wp-content/uploads/2017/08/D-Wave_LarryGoldstein_287_EXPORT.jpg?resize=480%2C480&ssl=1" width="500">

##### D-Wave Systems lançou comercialmente o 2000Q, um computador quântico de 2000 qubits


Em 2017, O físico brasileiro Guilherme Tosi, juntamente com uma equipe de pesquisadores da Universidade de Nova Gales do Sul, na Austrália, inventou uma nova arquitetura radical para a computação quântica, baseada em ‘flip-flop qubits’ que pode ser usada em um novo tipo de computador quântico permitindo, assim, a fabricação de processadores quânticos em larga escala pode se tornar muito mais barata – e mais fácil – do que se pensava ser possível, sem a necessidade do processo complicado da colocação precisa dos átomos de silício no processador.

## Quebras de paradigmas
A computação quântica quebra inúmeros paradigmas da computação clássica, na qual podemos dividir os problemas em "problemas tratáveis" e "problemas intratáveis".

Todos os elementos que mudam as estruturas clássicas vem das mudanças que a física clássica trouxe. Físicos como Heisenberg, Bohr, Schrödinger e Einstein estudaram esses novos fundamentos. Dentre eles podemos destacar:
* Sobreposição quântica

* Experiência do Gato de Schrödinger

* Entrelaçamento quântico ou "Ação fantasmagórica à distância"

* Teletransporte quântico

* Espalhamento de Rutherford

* Existência de multiverso

E foi graças a estes princípios que foi possível o desenvolvimento da Computação Quântica.

## Princípios da Computação Quântica
A Mecânica Quântica é considerada a mais bem sucedida teoria física. Pois desde a sua criação até os dias atuais, ela tem sido aplicada em diversos ramos, desde a física de partículas, atômica e molecular até a astrofísica e matéria condensada.

Na computação quântica a unidade de informação básica é o Bit quântico ou q-bit. O fato da computação quântica ser tão poderosa está no fato de que além de assumir '0' ou '1' como na computação clássica, ela pode assumir ambos os estados '0' e '1' ao mesmo tempo. Parece estranho algo assumir os dois estados diferentes ao mesmo tempo, mas a experiência mental do Gato de Schrödinger pode dar um sentido intuitivo à situação. E é graças à essa propriedade da superposição de estados que motivou os estudos em computação quântica. Se na computação clássica o processamento é sequencial, na computação quântica o processamento é simultâneo.
Imagine que uma pessoa precise encontrar o contato de telefone de alguma pessoa em uma lista, na computação clássica é como se ela olhasse em cada nome conferindo se é o contato procurado. Já em um processamento quântico é como se uma pessoa conseguisse conferir vários nomes a cada processamento.
O q-bit é descrito por um vetor estados em um sistema quântico de dois níveis o qual é equivalente a um vetor de espaço bidimensional sobre números complexos. Usa-se a notação de bra-ket para representá-los:

<img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/626a1534e007d6511762add99f012fbe7db8f845" width="200">

Assim, o estado de um q-bit pode ser representado por:

<img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/aabd1ffc6a57b00e254e4c212d98ebbea6ccc7fc" width="200">

O conjunto {| 0>,| 1>} forma uma base no espaço de Hilbert de duas dimensões, chamada de base computacional.
Para a manipulação dos estados quânticos utiliza-se principalmente técnicas ópticas, isto é radiação eletromagnética. Estes dispositivos constituem-se as portas lógicas quânticas. A manipulação pode ser realizada utilizando átomos que podem ser excitados ou não ou os dois ao mesmo tempo. Outro dispositivo utilizado é a manipulação de fótons. A vantagem em utilizá-los está no fato de que esses fótons podem constituir-se portadores altamente estáveis de informação quântica. Entretanto fótons não interagem diretamente entre si, sendo necessário o uso de um átomo como mediador, que introduz um ruído adicional e complicações no experimento. Neste caso um fóton interage com um átomo que por sua vez interage com o segundo fóton, levando à interação completa entre os dois fótons.
Para armazenar os q-bits utiliza-se armadilhas de íons (íon trap) em que um pequeno número de átomos carregados são aprisionados e armadilhas de átomos neutros (neutral íon trap) para aprisionar átomos sem carga.
Neste esquema os fótons são usados para manipular a informação contida nos átomos, dessa forma constituem um tipo de porta lógica quântica que aplica pulsos apropriados de radiação eletromagnética para que os átomos vizinhos possam interagir um com o outro como via, por exemplo, forças dipolo.
Outra classe de processamento de informação quântica é baseada na ressonância magnética nuclear (RMN). Neste caso a informação quântica é armazenada nos spins nucleares dos átomos em moléculas e as portas lógicas manipulam essa informação usando a radiação eletromagnética. Um pósitron ou elétron podem ter um spin pra cima, pra baixo e os dois ao mesmo tempo.
Os momentos magnéticos nucleares fazem um movimento natural de precessão na presença de campos magnéticos. Os estados quânticos dos núcleos podem ser manipulados irradiando os núcleos com pulsos de rádio frequência sintonizados na frequência de precessão destes.
Em um determinado composto constituído por diferentes átomos pode-se medir as ressonâncias dos núcleos de alguns átomos sem alterá-los utilizando a RMN. Ela é sensível às interações dos momentos nucleares expostos à campos elétricos e magnéticos locais, estas interações são chamadas de hiperfinas. Cada tipo de spin possui uma velocidade angular que depende do campo aplicado e da interação de troca entre eles.
Assim como na computação clássica, na computação quantica utiliza-se circuitos, porém esses circuitos são quânticos:

<img src="https://builtin.com/sites/default/files/styles/thumbnail/public/2020-08/duotone-68.png" width="700">

### Entrada:
Considera-se conjuntamente os qubits de entrada, matematicamente o que é chamado de seu produto tensorial;
### Linhas horizontais: 
As linhas que aparecem não são necessariamente fios. Elas representam a evolução de um qubit, podendo ser apenas a passagem do tempo ou, por exemplo, o deslocamento de um fóton;
### Sentido: 
O circuito descreve a evolução do sistema quântico no tempo, da esquerda para a direita;
### Linhas verticais: 
O segmento vertical informa que o circuito atua simultaneamente nos dois qubits. A linha vertical representa o sincronismo, e não o envio de informação;
### Controle:
Indica que o qubit representado nessa linha é um qubit de controle, ou seja, caso esteja no estado |1>, a porta realiza a operação; caso esteja no estado |0>, a porta não realiza operação alguma. Caso o qubit de controle seja um estado superposto ou os 2 qubits estejam emaranhados, não é possível compreender o comportamento individual do qubit de controle e do qubit alvo. Deve-se considerar a ação do operador unitário, que representa todo o circuito, atuando simultaneamente nos 2 qubits.
### Saída: 
Os qubits que compõem a saída do circuito podem ou não ser medidos. Como o qubit inferior está sendo medido, o resultado será 0 ou 1.
Como vemos, operações lógicas ou até mesmo algoritmos podem ser descritos por um circuito quântico. Nestes circuitos podemos utilizar as portas lógicas utilizadas na computação clássica, mas podemos utilizar ainda outras que chegam a permitir, por exemplo, a construção de um possível circuito para o teletransporte de dados.

Atualmente é possível realizar qualquer operação clássica utilizando somente portas Nand. O mesmo ocorre em circuitos quânticos onde as portas são: Hadanard (H), Controladora (CNOT), fase (S) e pi/8(T). Exemplos de portas quânticas:

### Porta NOT Quântica:
No caso clássico, a porta NOT troca o 1 por 0 e vice-versa. A generalização para o caso quântico é dada por um operador X que satisfaz

<img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/5b555a7d3e0a645bf98c7356e274b383ded2e48b" width="200">

Com isso, verifica-se facilmente que a representação matricial do operador X é dada por:

<img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/c035b52b7d1edac3e4d023933dcfedf16b505ea3" width="200">

Com a porta NOT quântica, existem situações sem contrapartida no caso clássico:

<img src="https://wikimedia.org/api/rest_v1/media/math/render/svg/12e57fa8f58f24e143d3eceedfccba23148b3028" width="200">

a saída será

<img src=" https://wikimedia.org/api/rest_v1/media/math/render/svg/f5bd6757975a117f3da2922fdc759f1dde904c99" width="200">

### Porta CNOT Quântica:
Atua em estados de 2 qubits, é a contrapartida quântica do circuito clássico da porta XOR. Ela tem 2 qubits de entrada, o de controle e o alvo. Uma porta controlada, age dependendo do valor do qubit de controle. Ela é "ativada" se o qubit de controle estiver no estado|1>, e nada faz, se ele estiver no estado |0>. Essa descrição é adequada apenas quando o qubit de controle está nos estados |0>, ou |1>. Entretanto, o que distingue a porta CNOT quântica da clássica é que, na porta CNOT quântica, os qubits alvo e de controle podem ser estados superpostos.
A ação da porta CNOT pode ser caracterizada pelas transformações operadas nos elementos da base computacional associada, ou seja,
<img src=" https://wikimedia.org/api/rest_v1/media/math/render/svg/1b20e1d4630fbe2e44b49fe816734b8b7c457c71" width="200">

Note que é possível representar essa ação na base computacional de forma mais esquemática por

<img src=" https://wikimedia.org/api/rest_v1/media/math/render/svg/e3d271fb11d2cbcd3f1d16dfc86d085349d96955" width="200">

onde I, j pertence {0,1} e §é a adição módulo 2.
Entretanto da mesma maneira que a superposição de estados permite a criação do computador quântico é essa mesma propriedade que inviabiliza a criação deles. A superposição é muito sensível a qualquer microruído eletromagnético que pode alterar o estado do q-bit fazendo com que a informação que ele continha seja perdida. Outro fato importante em questão é o superaquecimento das máquinas.


## Pesquisa hoje

<img src=" https://accmp.co.in/media/2020/07/quantum.jpg" width="700">

Ainda não existe hoje um computador inteiramente quântico funcionando. Porém, empresas como a canadense "D-Wave", entre outras tem feito promessas, que se forem cumpridas, anunciarão um novo horizonte para a realidade e o mercado de computadores domésticos, científicos e corporativos.
Muitos protótipos de computadores quânticos já foram testados em laboratórios de todo o mundo, porém o seu desenvolvimento em larga escala ainda pode estar distante. E dependente de muitas pesquisas e investimentos.
No Brasil há diversos núcleos de pesquisas na área da computação quântica. Há um grupo no LNCC (Laboratório Nacional de Computação Científica) formado por orientandos de projetos de iniciação científica, mestrado e doutorado. Além de grupos pertencentes as instituições de ensino superiores, com destaque para universidades do Rio de Janeiro e da Paraíba.
Os grupos do Brasil ligados a hardware quântico costumam desenvolver apenas pequenos protótipos, mas cooperam com os grandes pesquisadores estrangeiros da área.
As dificuldades de se criar um computador quântico reside no fato de que os processos computacionais passam a ser no universo atômico, que carece de tecnologias de manipulação ainda. Um dos principais problemas, por exemplo, é a alta taxa de erros causada pelo meio ambiente, devido a extrema sensibilidade da tecnologia.

## Conclusões
<img src="https://cdn-istoedinheiro-ssl.akamaized.net/wp-content/uploads/sites/17/2019/04/din1116-computacao1.jpg" width="1000">
Se analisada, a computação quântica pode ser encarada como um processo natural de evolução dos computadores, que a partir da década de 50, com o surgimento dos transistores, tem sua velocidade aumentada através da minituriarização de componentes. O limite físico destes componentes é justamente o tamanho quântico. O problema é que na escala quântica os conhecimentos da física clássica não podem ser aplicados.
Vale lembrar que para a maioria das aplicações convencionais os computadores atuais são eficientes. Porém para aplicações que requerem um processamento intenso (por exemplo: inteligência artificial, criptografia, busca em listas desordenadas, fatoração de números grandes) o computador quântico é a opção mais promissora. 

Segundo o físico Ivan Oliveira, do Centro de Pesquisas Físicas (CBPF), “Na teoria, computadores baseados em Qubits poderiam resolver problemas, que hoje levariam bilhões de anos, em questão de minutos.”

## Ver também:
Pedro Loss, Computadores Quânticos Explicados: https://www.youtube.com/watch?v=92eSz2X0AlU
## Referências
### Computação Quântica Wikipédia:
https://pt.wikipedia.org/wiki/Computa%C3%A7%C3%A3o_qu%C3%A2ntica

* Livro "A revolução dos Q-bits" - Ivan Oliveira
* Livro "O que é computação quantica?" - Galvão, Ernesto F.
* Livro "Computação Quântica e Informação Quântica" - Nielsen, Michael A.; Chuang, Isaac L.
* Texto "Computação Quântica" - Algretti, F.J.P.
* Texto "Computação Quântica e Informação Quântica" Oliveira, I.S. e Sarthour, R.S.
* M.A. Nielsen and I.L. Chuang, Quantum Computation and Quantum Information (Cambridge Press, 2001).
* D. Bouwmeester, A. Ekert and A. Zeilinger, The Physics of Quantum Information (Springer Verlag, 2001).
* C.P. Williams and S.H. Clearwater, Explorations in Quantum Computing (Springer Verlag, 1998).
* Victor Hugo da Costa Beserra (Petrópolis, 1998)

[ ⬅ Voltar ](.../README.MD)
