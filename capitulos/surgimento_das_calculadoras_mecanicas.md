[ ⬅ Voltar ](.../README.MD)

# Surgimento das calculadoras mecanicas

<img src="https://i.pinimg.com/originals/a5/92/8e/a5928e833f96ec521be5923d37b92d3d.jpg" width = 500>

# Primeira calculadora
 Acredita-se que a primeira calculadora da história tenha sido o ábaco, criado pelos chineses no século VI antes de Cristo. O ábaco é composto por fios paralelos e arruelas que deslizam sobre esses fios. No ábaco é possível realizar operações de subtração e adição. 
 Em 1642 o ábaco sofreu uma grande evolução, ocasionada pelo matemático francês Blaise Pascal. Com o intuito de ajudar e agilizar o trabalho do pai, Pascal inventou uma máquina automática de cálculos. Sua máquina possuía apenas as operações de subtração e adição.

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Arts_et_Metiers_Pascaline_dsc03869.jpg/1200px-Arts_et_Metiers_Pascaline_dsc03869.jpg" width = 200>

# Roda graduada

 Passados alguns anos o alemão Gottfried Wilhelm von Leibniz conseguiu, por meio de seus cálculos, projetar uma máquina que fosse capaz de realizar as demais operações. Esse instrumento recebeu o nome de “roda graduada”. Devido seu custo ainda ser bastante elevado, apenas algumas pessoas podiam utilizar esse instrumento.

<img src="https://upload.wikimedia.org/wikipedia/commons/9/92/Leibnitzrechenmaschine.jpg" width = 200>

# Primeira calculadora mecânica

 Então, em 1947, o austríaco Curt Herzstark desenvolveu o projeto da primeira calculadora mecânica, reduzida ao tamanho de um copo. O desenvolvimento ocorreu enquanto Curt era prisioneiro no campo de concentração nazista. Porém, ele só conseguiu concluir seu projeto quando foi liberto. As vendas de suas calculadoras duraram até 1973, quando surgiram as calculadoras eletrônicas.[1]

<img src="https://upload.wikimedia.org/wikipedia/commons/5/5c/Curta01.JPG" width = 200>

# Calculadoras atuais

 Nos dias atuais, as calculadoras, além de executarem operações aritméticas básicas, podem executar funções trigonométricas normais e inversas, além de poderem armazenar dados e instruções em registros de memórias, aproximando-a de computadores menores. Muitas calculadoras utilizam módulos pré-programados e reversíveis de software, com mais de 5 mil instruções.[2]


 <img src="https://castronaves.vteximg.com.br/arquivos/ids/167672-1000-1000/57836.jpg?v=636620800477470000" width = 200>

# Referências:

1. A história da calculadora; http://clickeaprenda.uol.com.br/portal/mostrarConteudo.php?idPagina=29427
2. A história da calculadora; http://www.clickideia.com.br/portal/conteudos/c/29/21666

[ ⬅ Voltar ](.../README.MD)

